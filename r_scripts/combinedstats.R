library(readr)
library(dplyr)
library(ggplot2)
library(scales)
###COMBINED STATS
#DatasetStats
clusters <- c("diff","20")

balanced <- c("b","im")
for(cluster in clusters){
  for(balance in balanced){
    print(paste0("PRINT COMBINED DATASET STATS"))
    f <- paste0("c:/exp/combined_datasetstats_",balance,"_",cluster,".csv")
    if(file.exists(f) && file.size(f)>0){
      locps <- read.csv(file = f,header = T,sep = ";",row.names = NULL)
      
      locps <- data.frame(locps,row.names = NULL)
      
      #glimpse(locps)
      
      
      
      a <- ggplot(data=locps,aes(x=commit,y=Scumble, colour=Level))+
        geom_line()+
        scale_color_brewer(palette="Dark2")+
        facet_wrap(.~ project)
      #labs(title="scumble per commit")
      
      ggsave(paste0("c:/exp/plots/combined_",balance,"_",cluster,"_scumble_lineplot.pdf"),width = 7, height = 4)
      
      #Instances
      a <- ggplot(data=locps,aes(x=commit,y=NumOfInstances, colour=Level))+
        geom_line()+
        scale_color_brewer(palette="Dark2")+
        facet_wrap(.~ project)
      #labs(title="scumble per commit")
      
      ggsave(paste0("c:/exp/plots/combined_",balance,"_",cluster,"_instances_lineplot.pdf"),width = 9, height = 6)
      
      #ggplot(locps,aes(x=Classifier,y=AveragePrecisionForAllNonNullPrecision))+
       # geom_boxplot(aes(colour=feature))+
       # labs(y=paste0("precision"), x="")
      
      ggplot(locps,aes(x=Level,y=Scumble))+
        geom_boxplot(aes(colour=project))+
        scale_color_brewer(palette="Dark2")+
        labs(y=paste0("scumble"),x="gran. level")
      ggsave(paste0("c:/exp/plots/combined_",balance,"_",cluster,"_scumble_boxplot.pdf"),width = 4, height = 2)
      
      
      b <- ggplot(data=locps,aes(x=commit,y=MeanIR, colour=Level))+
        geom_line()+
        facet_wrap(.~ project)
      #labs(title = "mean imbalance ratio per commit")
      
      ggsave(paste0("c:/exp/plots/combined_",balance,"_",cluster,"_meanIR_lineplot.pdf"),width = 7, height = 4)
      
      ggplot(locps,aes(x=commit,y=MeanIR,group=Level, colour=project))+
        geom_boxplot(aes(x = Level))+
        scale_color_brewer(palette="Dark2")+
        labs(y=paste0("MeanIR across all commits"))
      
      ggsave(paste0("c:/exp/plots/combined_",balance,"_",cluster,"_meanIR_boxplot.pdf"),width = 7, height = 4)
    }
  }#end for loop fr balance dataset stats
}#end forloop for clusters dataset stats