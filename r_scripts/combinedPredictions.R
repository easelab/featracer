library(readr)
library(dplyr)
library(ggplot2)
library(scales)
#library(ggpubr)


#COMBINED predictions and Cross validation

normalized <- c("reg")
nfeatures <- c("4f","6f","8f","11f")
projects <- c("viz","ide","config","tools","marlin")
balanced <- c("im")
codeLevels <- c("loc","fragment","file","folder")

#for(features in nfeatures){
for(norm in normalized){
  for(project in projects){
    for(balance in balanced){
      for(codeLevel in codeLevels){
        print(codeLevel)
        ###PREDICTIONS
        print(paste0(cluster,norm,features,project,"_",codeLevel,"_",balance,"_PEDICTIONS"))
        f <- paste0("c:/exp/combined_",project,"_",codeLevel,"_ps_",balance,"_",norm,".csv")
        if(file.exists(f) && file.size(f)>0){
          locps <- read.csv(file = f,header = T,sep = ";",row.names = NULL)
          
          locps <- data.frame(locps,row.names = NULL)
          
          #glimpse(locps)
          
          #AVERAGE PRECISION (Prediction)
          ggplot(locps,aes(x=Classifier,y=AveragePrecisionForAllNonNullPrecision))+
            geom_boxplot(aes(colour=feature))+
            scale_color_brewer(palette="Dark2")+
            labs(y=paste0("precision"), x="")+
            theme_bw()+
            theme(legend.position="top",legend.title = element_blank())
          
          ggsave(paste0("c:/exp/plots/combined_",project,"_",codeLevel,"_",balance,"_",norm,"_predict_boxplot.pdf"), width = 9,height = 6)
          
          
          ggplot(data=locps,aes(x=factor(commit,level=nfeatures),y=AveragePrecisionForAllNonNullPrecision,group=Classifier,colour=features))+
            geom_line(show.legend = FALSE)+
            facet_wrap(.~ Classifier)+
            theme_bw()+
            labs(y=paste0("precision"))
          
          ggsave(paste0("c:/exp/plots/combined_",project,"_",codeLevel,"_",balance,"_",norm,"_predict_lineplot.pdf"), width = 9,height = 3)
        }
        #cross validation plots
        
        ###CROSS VALIDATION: only plot balanced cross validation
        print(paste0(project,"_",codeLevel,"_",balance,"_CROSS VALIDATION"))
        
        f <- paste0("c:/exp/combined_",project,"_",codeLevel,"_cv_",balance,"_",norm,"_",features,".csv")
        if(file.exists(f) && file.size(f)>0){
          cv <- read.csv(file = f,header = T,sep = ";",row.names = NULL)
          cv <- data.frame(cv,row.names = NULL)
          
          #glimpse(cv)
          
          #subset accuracy
          
          
          ggplot(cv,aes(x=commit,y=SubsetAccuracy,group=Classifier,group=Classifier,colour=clusters))+
            geom_boxplot(aes(x = Classifier))+
            labs(y=paste0("subset accuracy"),x="")
          
          ggsave(paste0("c:/exp/plots/combined_",project,"_",codeLevel,"_",balance,"_",norm,"_",cluster,"_",features,"_cv_boxplot_SA.pdf"), width = 7,height = 3)
          print("DONE Subset accuracy box plot")
          #hamming loss
          ggplot(cv,aes(x=commit,y=HammingLoss,group=Classifier,colour=clusters))+
            geom_boxplot(aes(x = Classifier))+
            labs(y=paste0("hamming loss"),x="")
          
          ggsave(paste0("c:/exp/plots/combined_",project,"_",codeLevel,"_",balance,"_",norm,"_",cluster,"_",features,"_cv_boxplot_HL.pdf"), width = 7,height = 3)
          print("DONE haming loss box plot")
          #Subset accuracy line plot
          
          ggplot(data=cv,aes(x=commit,y=SubsetAccuracy,group=Classifier,colour=clusters))+
            geom_line()+
            facet_wrap(.~ Classifier)+
            labs(y=paste0("subset accuracy",x=""))
          
          ggsave(paste0("c:/exp/plots/",project,"_",codeLevel,"_",balance,"_",norm,"_",cluster,"_",features,"_cv_lineplot_SA.pdf"), width = 7,height = 4)
          
          print("DONE Subset accuracy box plot")
        }
        
        
        #figure <- ggarrange(a,b,
        
        #                    ncol=1,nrows=2)
        
        
        #ggexport(figure,filename=paste0("c:/exp/plots/",codeLevel,"_",balance,"_datasetstats_lineplot.pdf"))
      }
      #figure <- ggarrange(a,b,
      
      #                    ncol=1,nrows=2)
      
      
      #ggexport(figure,filename=paste0("c:/exp/plots/combined_",balance,"_datasetstats_lineplot.pdf"))
    }#end loop for datasetType
  }#end of project loop
}#end loop for normalised or regular
#}#end loop for nFeatures
