library(readr)
library(dplyr)
library(ggplot2)
library(scales)
#library(ggpubr)


#COMBINED predictions and Cross validation

codeLevels <- c("files","fragments","loc")


        for(codeLevel in codeLevels){
          print(codeLevel)
          ###PREDICTIONS
          print(paste0("allprojects_measures_",codeLevel,".csv"))
          f <- paste0("c:/exp/allprojects_measures_",codeLevel,".csv")
          if(file.exists(f) && file.size(f)>0){
            locps <- read.csv(file = f,header = T,sep = ";",row.names = NULL)
            
            locps <- data.frame(locps,row.names = NULL)
            
            #glimpse(locps)
            
            #AVERAGE PRECISION (Prediction)
            ggplot(data=locps,aes(x=commit,y=measurevalue,colour=measure))+
              geom_line(aes(linetype=measure),size=0.1)+
              facet_wrap(.~ project)+
              labs(y="")+
              theme_bw()+
              theme(legend.position="top",legend.title = element_blank(),,legend.margin=margin(0,0,0,0),
                    legend.box.margin=margin(-10,-10,-10,-10))
            
            
            ggsave(paste0("c:/exp/plots/allprojects_measures_",codeLevel,"_lineplot.pdf"), width = 6,height = 3)
            
            level_order <- c('config','ide','tools','viz','marlin')
            ggplot(data=locps,aes(x= factor(project,level=level_order),y=measurevalue))+
              geom_boxplot(aes(colour=measure))+
              scale_color_brewer(palette="Dark2")+
              #facet_wrap(.~Classifier)+
              labs(y="",x="",fill="")+
              theme_bw()+
              
              theme(legend.position="top",legend.title = element_blank())
            
            ggsave(paste0("c:/exp/plots/allprojects_measures_",codeLevel,"_boxplot.pdf"), width = 6,height = 3)
          }
          
        
        }
     