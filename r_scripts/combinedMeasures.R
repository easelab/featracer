library(readr)
library(dplyr)
library(ggplot2)
library(scales)
#library(ggpubr)


#COMBINED predictions and Cross validation

normalized <- c("norm")
nfeatures <- c("4f")
projects <- c("viz","ide","config","tools")
balanced <- c("im")
codeLevels <- c("loc","fragment","file","folder")

for(features in nfeatures){
#for(norm in normalized){
  for(project in projects){
    #for(balance in balanced){
      for(codeLevel in codeLevels){
        print(codeLevel)
        ###PREDICTIONS
        print(paste0(cluster,norm,features,project,"_",codeLevel,"_",balance,"_PEDICTIONS"))
        f <- paste0("c:/exp/combined_measures_",project,"_",codeLevel,"_ps_im_norm_diff_",features,".csv")
        if(file.exists(f) && file.size(f)>0){
          locps <- read.csv(file = f,header = T,sep = ";",row.names = NULL)
          
          locps <- data.frame(locps,row.names = NULL)
          
          #glimpse(locps)
          
          #AVERAGE PRECISION (Prediction)
          ggplot(data=locps,aes(x=commit,y=measurevalue,colour=measure))+
            geom_line(aes(linetype=measure),size=0.3)+
            facet_wrap(.~ Classifier)+
            labs(y="")+
            theme(legend.position="top",legend.title = element_blank())
          
          
          #ggplot(locps,aes(x=Classifier,y=AveragePrecisionForAllNonNullPrecision))+
            #geom_boxplot(aes(colour=feature))+
            #labs(y=paste0("precision"), x="")
          
          ggsave(paste0("c:/exp/plots/combined_measures_",project,"_",codeLevel,"_",balance,"_norm__diff_",features,"_predict_lineplot.pdf"), width = 4,height = 3)
          
          ggplot(data=locps,aes(x=Classifier,y=measurevalue))+
            geom_boxplot(aes(colour=measure))+
            #facet_wrap(.~Classifier)+
          labs(y="",x="",fill="")+
            
            theme(legend.position="top",legend.title = element_blank())
          #ggplot(data=locps,aes(x=commit,y=AveragePrecisionForAllNonNullPrecision,group=Classifier,colour=features))+
            #geom_line(show.legend = FALSE)+
            #facet_wrap(.~ Classifier)+
            #labs(y=paste0("precision"))
          
          ggsave(paste0("c:/exp/plots/combined_measures_",project,"_",codeLevel,"_",balance,"_norm_diff_",features,"_predict_boxplot.pdf"), width = 4,height = 3)
        }
        
        
        #figure <- ggarrange(a,b,
        
        #                    ncol=1,nrows=2)
        
        
        #ggexport(figure,filename=paste0("c:/exp/plots/",codeLevel,"_",balance,"_datasetstats_lineplot.pdf"))
      }
      #figure <- ggarrange(a,b,
      
      #                    ncol=1,nrows=2)
      
      
      #ggexport(figure,filename=paste0("c:/exp/plots/combined_",balance,"_datasetstats_lineplot.pdf"))
    #}#end loop for datasetType-balanced or not
  }#end of project loop
#}#end loop for normalised or regular
}#end loop for nFeatures
