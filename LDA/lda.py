# This script performs LDA and saves its results and models for each commit of a project to several files, one directory per commit

from pprint import pprint
import re
import nltk
import nlp 
import pandas as pd
import numpy as np
import pickle
import os

# stopwords
from nltk.corpus import stopwords

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim.test.utils import datapath

# spacy for Lemmatization
import spacy
import en_core_web_sm

# plotting
import pyLDAvis
import pyLDAvis.gensim
import matplotlib.pyplot as plt


# Constants
NUMBER_OF_TOPICS = 5                                    # The number of topics the model will work with
ALLOWED_POSTAGS = ['NOUN', 'VERB', 'ADJ', 'ADVERB']               # Kind of words that should not be removed from the documents


newpath = ""


# Preprocess the dataset by removing characters
def preprocess(df):
    # Convert to list
    data = df

    # Remove Emails
    data = [re.sub('\S*@\S*\s?', '', i) for i in data]

    # Remove new line characters
    data = [re.sub('\s+', ' ', i) for i in data]

    # Remove single quotes
    data = [re.sub("\'", "", i) for i in data]

    return data


# Convert sentence to words
def sentences_to_words(sentences):
    for sentence in sentences:
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))


# Delete stopwords from dataset
def remove_stopwords(texts):
    stop_words = stopwords.words('english')
    return [[word for word in simple_preprocess(str(doc)) 
            if word not in stop_words] for doc in texts]


# Lemmatize dataset i.e. remove all words that do not have a given postags
def lemmatization(texts, allowed_postags=ALLOWED_POSTAGS):
    nlp = en_core_web_sm.load()
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out


# Compute coherence values in a range of topics
def compute_coherence_values(dictionary, corpus, texts, limit, start=2, step=1):
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        model = gensim.models.LdaMulticore(corpus=corpus,
                                            id2word=dictionary,
                                            num_topics=num_topics,
                                            random_state=100,
                                            chunksize=200,
                                            passes=10,
                                            per_word_topics=True)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())

    return model_list, coherence_values


# Compute coherence value given a number of topics, alpha and eta value
def compute_coherence_values(dictionary, texts, k, a, b):
    lda_model = gensim.models.LdaMulticore(corpus=texts,
                                            id2word=dictionary,
                                            num_topics=k,
                                            random_state=100,
                                            chunksize=200,
                                            passes=10,
                                            alpha=a,
                                            eta=b,
                                            per_word_topics=True)
    
    coherencemodel = CoherenceModel(model=lda_model, texts=texts, dictionary=dictionary, coherence='c_v')
    return coherencemodel.get_coherence()


# Save a model to external file
def save_model(model, name):
    temp_file = model.save(newpath + name)


# Load a saved model
def load_model(name):
    temp_file = datapath(name)
    lda_model = gensim.models.LdaModel.load(temp_file)
    return lda_model


# Print topics and words to txt file
def topics_to_file(model):
    with open(newpath + "\output.txt", 'w') as f:
        for line in model.print_topics():
            f.write(str(line[0]) + "\n")
            f.write(line[1] + "\n")


# Train a model
# file - Filepath (txt or csv) of dataset containing documents
# name - Name of save-file containing trained model
def train(documents, name, topic_count = NUMBER_OF_TOPICS):
    print("Loading data...")
    #labels, documents = parse_csv(file, 'category', 'text')
    # labels, documents = parse_txt(file)

    # Preprocess data
    data = preprocess(documents)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_words_nostops)#data_lemmatized

    # Create Corpus
    texts = data_words_nostops#data_lemmatized

    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]

    # Baseline model
    #lda_model = gensim.models.LdaMulticore(corpus=corpus,
    #                                            id2word=id2word,
    #                                            num_topics=2,
    #                                            random_state=100,
    #                                            chunksize=200,
    #                                            passes=10,
    #                                            per_word_topics=True)
    
    # Uncomment this to calculate coherence values
    # model_list, coherence_values = compute_coherence_values(dictionary=id2word, corpus=corpus, 
    #                                                        texts = data_lemmatized, start=2, limit=8, step=1)
    print("Training model...")
    # Optimal model (for BBC dataset)
    lda_model = gensim.models.LdaMulticore(corpus=corpus,
                                            id2word=id2word,
                                            num_topics=topic_count,
                                            random_state=100,
                                            chunksize=200,
                                            passes=20,
                                            iterations=400,
                                            alpha=0.01,
                                            eta='symmetric',
                                            per_word_topics=True,
                                            workers=4)
    
   
    save_model(lda_model, name)
    topics_to_file(lda_model)
    #print(lda_model.print_topics())

    # Create Visualisation for training model
    ldavis_prepared = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
    pyLDAvis.save_html(ldavis_prepared, newpath + 'LDA_Visualisation_train.html')

    return lda_model


def test(lda_model, documents):
    # Load dataset
    print("Loading test data...")
    #gt_labels, documents = parse_csv(file, 'category', 'text') 
    # gt_labels, issues = parse_txt(file)

    # Preprocess data
    data = preprocess(documents)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_words_nostops)#data_lemmatized

    # Create Corpus
    texts = data_words_nostops#data_lemmatized

    # Term Document Frequency 
    corpus = [id2word.doc2bow(text) for text in texts]

    # Uncomment to save corpus to file
    # with open("corpusBBC.pk", 'wb') as file:
    #    pickle.dump(corpus, file)

    # Uncomment to load corpus from file
    # corpus = []
    # with open("corpusBBC.pk", 'rb') as file:
    #    corpus = pickle.load(file)

    # Create visualization of test data
    # ldavis_prepared = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
    # pyLDAvis.save_html(ldavis_prepared, 'C:/Users/hermann/Documents/Projects/Feature Location/LDA_Visualisation_test.html')

    # Predict label for all topics
    estimate = lda_model.get_document_topics(corpus, 0.1)

    # Uncomment to save predictions to file
    # with open("estimateBBC.pk", 'wb') as file:
    #    pickle.dump(estimate, file)

    # Evaluate model (Calculate Precision, Recall and F_score)
    with open(newpath + "mapping.txt", 'w') as f:        
        for i in range(len(corpus)):
            # Write topic assignment to file
            f.write(data[i] + " \n")
            f.write(str(estimate[i]) + "\n")


# Parse txt file and extract labels and documents
def parse_txt(file):
    features = []
    content = []
    assets = []
    with open(file, 'r') as f:
        next(f)
        for line in f:
            line_split = line.split(';')
            assets.append(line_split[0])
            labels = line_split[1].split(',')
            for feature in labels:
                feature = feature.replace(" ", "")
                features.append(feature)
            feature_count = len(set(features))
            content.append(line_split[2])
    return assets, labels, content, feature_count


# Parse csv file and extract labels and documents
def parse_csv(file, labels, documents):
    df = pd.read_csv(file)
    return df[labels], df[documents]


if __name__ == '__main__':
    # Update stopwords
    nltk.download('stopwords')

    blender_datasets = pd.read_csv(r"tse\LDA\datasets\blender_datasets.csv", delimiter=';')
    train_path = blender_datasets['train']
    test_path = blender_datasets['test']

    for line in range(len(blender_datasets['project'])):
        newpath = r"C:\Users\Hermann\Documents\PaperProjects\2020-icse-featracer\tse\LDA\results\blender\blender_" + str(line) + '\\'
        if not os.path.exists(newpath):
            os.makedirs(newpath)

        print("Training on set " + str(line))
        assets, labels, content, feature_count = parse_txt(blender_datasets['train'][line])

        model = train(content, 'blender_test', feature_count)
        print("Testing on set " + str(line))
        assets, labels, content, feature_count = parse_txt(blender_datasets['test'][line])

        test(model, content)
