# This script calculates the confusion matrix values as well as precision, recall and f-score for a given dataset and saves the values in a csv file

import pandas as pd
import os


def parse_txt(file, queries_path = r'queries\\'):
    features = []
    content = []
    assets = []
    labels = []
    with open(file, 'r') as f:
        next(f)
        for line in f:
            line_split = line.split(';')
            assets.append(line_split[0])
            labels.append(line_split[1].split(', '))
            for l in labels:
                for feature in l:
                    feature = feature.replace(" ", "")
                    features.append(feature)
            feature_count = len(set(features))
            content.append(line_split[2])
    return assets, labels, content, feature_count, set(features)



source_directory = r"D:\results\parrot\\"
results_directory = source_directory + r'results\\'
queries_directory = source_directory + r'queries\\'
docs_directory = source_directory + r'docs\\'

dataset_csv = r"C:\fanas\2\lda_data\parrot_datasets.csv"
datasets = pd.read_csv(dataset_csv, delimiter=';')
test_path = datasets['test']

threshold = 0.01
tp = 0
fp = 0
tn = 0
fn = 0

for commit in os.listdir(source_directory):
    
    commit_directory = source_directory + commit + r'\\'
    results_directory = commit_directory + r'results\\'
    queries_directory = commit_directory + r'queries\\'
    docs_directory = commit_directory + r'docs\\'

    assets, labels, content, feature_count, features = parse_txt(test_path[int(commit)])

    for file in os.listdir(results_directory):
        if os.path.getsize(results_directory + file) > 0:
            with open(results_directory + file, 'r') as f:
                for line in f:
                    line = line.replace('\n', '')
                    line = line.replace('.txt', '')
                    line = line.split(r',', 1)
                    with open(queries_directory + file, 'r') as g:
                        query = g.readline()
                        for l in labels[int(line[0])]:
                            if l == query:
                                tp += 1
                            else:
                                fp += 1
        else:
            with open(queries_directory + file, 'r') as g:
                query = g.readline()
                for l in labels:
                    for k in l:
                        if query == k:
                            fn += 1
                        else:
                            tn += 1

precision = tp/(tp + fp)
recall = tp/(tp + fn)
if tp == 0:
    f_score = 0
else:
    f_score = (2 * precision * recall) / (precision + recall)

print("True positives: " + str(tp))
print("False positives: " + str(fp))
print("True negatives: " + str(tn))  
print("False negatives: " + str(fn))
print('\n')
print("Precision: " + str(precision))  
print("Recall: " + str(recall))
print("F1-Score: " + str(f_score))

with open("results.csv", 'a') as r:
    r.write("parrot;" + str(tp) + ';' + str(fp) + ';' + str(tn) + ';' + str(fn) + ';' + str(precision) + ';' + str(recall) + ';' + str(f_score) + '\n')