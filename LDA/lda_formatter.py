# This script generates the queries and converts the data of the dataset and LDA results to convert it in a format that lucene can work with

from pprint import pprint
import re
import nltk
import nlp 
import pandas as pd
import numpy as np
import pickle
import os

# stopwords
from nltk.corpus import stopwords
import keyword

# Gensim
import gensim
import gensim.corpora as corpora
from gensim.utils import simple_preprocess
from gensim.models import CoherenceModel
from gensim.test.utils import datapath
from gensim.test.utils import common_texts
from gensim.test.utils import Dictionary

# spacy for Lemmatization
import spacy
import en_core_web_sm

# plotting
import pyLDAvis
import pyLDAvis.gensim
import matplotlib.pyplot as plt

import subprocess


# Constants
NUMBER_OF_TOPICS = 5                                    # The number of topics the model will work with
ALLOWED_POSTAGS = ['NOUN', 'VERB', 'ADJ']               # Kind of words that should not be removed from the documents

python_stopwords = keyword.kwlist
java_stopwords = ["abstract", "new",          "switch",
        "assert",   "default",      "package",      "synchronized",
        "boolean",  "do",           "goto",         "private",      "this",
        "double",       "implements",   "protected",    "throw",
        "byte",     "public",       "throws",
        "case",     "enum",         "instanceof",   "transient",
        "catch",    "extends",      "int",          "short",        "try",
        "char",     "final",        "interface",    "static",       "void",
        "class",    "finally",      "long",         "strictfp",     "volatile",
        "const",    "float",        "native",       "super",
        "bool", "package"]
c_stopwords = ["case", "template", "namespace", "delete", "char8_t", "char16_t", "char32_t", "auto", "const", "struct", "unsigned", "signed", "switch", "default", "register", "sizeof", "typedef", "union", "extern",
               "alignas", "alignof", "and_eq", "asm", "atomic_cancel", "atomic_commit", "atomic_noexcept", "bitan", "bitor", "compl", "concept", "consteval", "constexpr", "constinit", "const_cast", "co_await", "co_return", "co_yield",
               "decltype", "dynamic_cast", "explicit", "export", "friend", "inline", "mutable", "noexcept", "not_eq", "nullptr", "operator", "or_eq", "reflexpr", "register", "reinterpre_cast", "requires", "static_assert", "static_cast",
               "typeid", "typename", "using", "virtual", "wchar_t", "xor", "xor_eq", "ifdef", "ifndef", "define", "undef", "endif"]
#custom_stopwords = ['return', 'var', 'null', 'true', 'false', 'lambda', 'class', 'global', 'break']

newpath = ""

# Preprocess the dataset by removing characters
def preprocess(df):
    # Convert to list
    data = df
    processed_data = []

    for i in data:
        # Remove Emails
        i = re.sub('\S*@\S*\s?', '', i)
        
        # Remove single quotes
        i = re.sub("\'", "", i)

        # Split camel case
        i = re.sub('([A-Z][a-z]+)', r' \1', re.sub('([A-Z]+)', r' \1', i))

        # Convert to lower case
        i = i.replace('_', ' ').lower()

        # Remove numbers
        i = re.sub('[0-9]', " ", i)

        # Remove newlines, tabs and spaces
        i = re.sub('[\n\t\s]+', " ", i)

        # Remove new line characters
        i = re.sub('\s+', ' ', i)

        # Remove null
        i = re.sub('null', '', i)

        for word in python_stopwords:           
            i = re.sub(r'(?<![a-zA-Z])' + word + '(?![a-z-Z])', '', i)
        
        for word in java_stopwords:
            i = re.sub(r'(?<![a-zA-Z])' + word + '(?![a-z-Z])', '', i)

        for word in c_stopwords:
            i = re.sub(r'(?<![a-zA-Z])' + word + '(?![a-z-Z])', '', i)
        
        processed_data.append(i)

    return processed_data


# Convert sentence to words
def sentences_to_words(sentences):
    for sentence in sentences:
        yield(gensim.utils.simple_preprocess(str(sentence), deacc=True))


# Delete stopwords from dataset
def remove_stopwords(texts):
    stop_words = stopwords.words('english')
    return [[word for word in simple_preprocess(str(doc)) 
            if word not in stop_words] for doc in texts]


# Lemmatize dataset i.e. remove all words that do not have a given postags
def lemmatization(texts, allowed_postags=ALLOWED_POSTAGS):
    nlp = en_core_web_sm.load()
    texts_out = []
    for sent in texts:
        doc = nlp(" ".join(sent))
        texts_out.append([token.lemma_ for token in doc if token.pos_ in allowed_postags])
    return texts_out


# Compute coherence values in a range of topics
def compute_coherence_values(dictionary, corpus, texts, limit, start=2, step=1):
    coherence_values = []
    model_list = []
    for num_topics in range(start, limit, step):
        model = gensim.models.LdaMulticore(corpus=corpus,
                                            id2word=dictionary,
                                            num_topics=num_topics,
                                            random_state=100,
                                            chunksize=200,
                                            passes=10,
                                            per_word_topics=True)
        model_list.append(model)
        coherencemodel = CoherenceModel(model=model, texts=texts, dictionary=dictionary, coherence='c_v')
        coherence_values.append(coherencemodel.get_coherence())

    return model_list, coherence_values


# Compute coherence value given a number of topics, alpha and eta value
def compute_coherence_values(dictionary, texts, k, a, b):
    lda_model = gensim.models.LdaMulticore(corpus=texts,
                                            id2word=dictionary,
                                            num_topics=k,
                                            random_state=100,
                                            chunksize=200,
                                            passes=10,
                                            alpha=a,
                                            eta=b,
                                            per_word_topics=True)
    
    coherencemodel = CoherenceModel(model=lda_model, texts=texts, dictionary=dictionary, coherence='c_v')
    return coherencemodel.get_coherence()


# Save a model to external file
def save_model(model, name):
    temp_file = model.save(newpath + name)


# Load a saved model
def load_model(name):
    temp_file = datapath(name)
    lda_model = gensim.models.LdaModel.load(temp_file)
    return lda_model


# Print topics and words to txt file
def topics_to_file(model):
    with open(newpath + "topics.txt", 'w') as f:
        for line in model.print_topics(num_topics=-1):
            f.write(str(line[0]) + "\n")
            f.write(line[1] + "\n")


# Train a model
# file - Filepath (txt or csv) of dataset containing documents
# name - Name of save-file containing trained model
def train(documents, name, topic_count = NUMBER_OF_TOPICS):
    print("Loading data...")
    #labels, documents = parse_csv(file, 'category', 'text')
    # labels, documents = parse_txt(file)

    # Preprocess data
    data = preprocess(documents)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = data_words_nostops#lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized

    # Term Document Frequency
    corpus = [id2word.doc2bow(text) for text in texts]

    # Baseline model
    #lda_model = gensim.models.LdaMulticore(corpus=corpus,
    #                                            id2word=id2word,
    #                                            num_topics=2,
    #                                            random_state=100,
    #                                            chunksize=200,
    #                                            passes=10,
    #                                            per_word_topics=True)
    
    # Uncomment this to calculate coherence values
    # model_list, coherence_values = compute_coherence_values(dictionary=id2word, corpus=corpus, 
    #                                                        texts = data_lemmatized, start=2, limit=8, step=1)
    print("Training model...")
    # Optimal model (for BBC dataset)
    try:
        lda_model = gensim.models.LdaMulticore(corpus=corpus,
                                                id2word=id2word,
                                                num_topics=topic_count,
                                                random_state=100,
                                                chunksize=200,
                                                passes=20,
                                                iterations=50,
                                                alpha=0.01,
                                                eta='symmetric',
                                                per_word_topics=True,
                                                workers=7)
        save_model(lda_model, name)
        topics_to_file(lda_model)

        return lda_model
    except ValueError:
        print("Catched ValueError: cannot compute LDA over an empty collection (no terms)")
    except:
        print("Something went wrong")
        with open(newpath + "error.txt", 'w') as f:
            f.write("Something went wrong here")
    
    #print(lda_model.print_topics())

    # Create Visualisation for training model
    #ldavis_prepared = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
    #pyLDAvis.save_html(ldavis_prepared, newpath + 'LDA_Visualisation_train.html')


def test(lda_model, documents):
    # Load dataset
    print("Loading test data...")
    #gt_labels, documents = parse_csv(file, 'category', 'text') 
    # gt_labels, issues = parse_txt(file)

    # Preprocess data
    data = preprocess(documents)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = data_words_nostops#lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized

    # Term Document Frequency 
    corpus = [id2word.doc2bow(text) for text in texts]

    # Uncomment to save corpus to file
    # with open("corpusBBC.pk", 'wb') as file:
    #    pickle.dump(corpus, file)

    # Uncomment to load corpus from file
    # corpus = []
    # with open("corpusBBC.pk", 'rb') as file:
    #    corpus = pickle.load(file)

    # Create visualization of test data
    # ldavis_prepared = pyLDAvis.gensim.prepare(lda_model, corpus, id2word)
    # pyLDAvis.save_html(ldavis_prepared, 'C:/Users/hermann/Documents/Projects/Feature Location/LDA_Visualisation_test.html')

    # Predict label for all topics
    estimate = lda_model.get_document_topics(corpus, 0.0000000000)

    # Uncomment to save predictions to file
    # with open("estimateBBC.pk", 'wb') as file:
    #    pickle.dump(estimate, file)

    # Uncomment to load predictions from file
    # estimate = []
    # with open("estimateBBC.pk", 'rb') as file:
    #    estimate = pickle.load(file)

    # Evaluate model (Calculate Precision, Recall and F_score)
    with open(newpath + "estimation.txt", 'w') as f:
        # Initialize confusion matrix
        confusion_matrix = np.zeros((NUMBER_OF_TOPICS, NUMBER_OF_TOPICS))
        
        for i in range(len(corpus)):
            # Write topic assignment to file
            f.write(data[i] + " \n")
            f.write(str(estimate[i]) + "\n")

            # Print progress
            # progress = int(i / len(gt_labels) * 100)
            # print(str(progress) + "%")
            continue
            # Select topic with highest probability
            max_prob = 0
            max_topic = 0
            for est_label in estimate[i]:
                if est_label[1] > max_prob:
                    max_topic = est_label[0]
                    max_prob = est_label[1]

            # Translate GT topic to index TODO: Hardcoded as of now. Find a way to generalize this
            index = 0
            if gt_labels[i] == 'politics':
                index = 1
            elif gt_labels[i] == 'entertainment':
                index = 2
            elif gt_labels[i] == 'sport':
                index = 3
            elif gt_labels[i] == 'business':
                index = 4
            
            confusion_matrix[index][int(max_topic)] += 1
            max_prob = 0
            max_topic = 0
        '''
        # Calculate metrics for each topic
        sum_tp = 0
        sum_fp = 0
        sum_fn = 0

        precision_array = []
        recall_array = []
        f_score_array = []

        # Calculate true positives, false positives and false negatives for each topic
        for i in range(len(confusion_matrix)):
            for j in range(len(confusion_matrix)):
                if i == j:
                    sum_tp += confusion_matrix[i][j]
                else:
                    sum_fp += confusion_matrix[i][j]
            for k in range(len(confusion_matrix)):
                if i != k:
                    sum_fn += confusion_matrix[k][i]

            precision = sum_tp / (sum_tp + sum_fp)
            recall = sum_tp / (sum_tp + sum_fn)
            f_score = 2 * ((precision * recall) / (precision + recall))

            precision_array.append(precision)
            recall_array.append(recall)
            f_score_array.append(f_score)
        

        print(confusion_matrix)
        print(precision_array)
        print(recall_array)
        print(f_score_array)
        '''


# Parse txt file and extract labels and documents
def parse_txt(file, queries_path = r'queries\\'):
    features = []
    content = []
    assets = []
    with open(file, 'r') as f:
        next(f)
        for line in f:
            line_split = line.split(';')
            assets.append(line_split[0])
            labels = line_split[1].split(',')
            for feature in labels:
                feature = feature.replace(" ", "")
                features.append(feature)
            feature_count = len(set(features))
            content.append(line_split[2])
    return assets, labels, content, feature_count, set(features)


# Parse csv file and extract labels and documents
def parse_csv(file, labels, documents):
    df = pd.read_csv(file)
    return df[labels], df[documents]

def SortTuple(tup):
     
    # Getting the length of list
    # of tuples
    n = len(tup)
     
    for i in range(n):
        for j in range(n-i-1):
             
            if tup[j][0] > tup[j + 1][0]:
                tup[j], tup[j + 1] = tup[j + 1], tup[j]
                 
    return tup


if __name__ == '__main__':
    # Update stopwords
    max_lines = {'gnumeric_datasets.csv': 646,'emacs_datasets.csv': 220, 'config_datasets.csv': 127, 'ide_datasets.csv': 195, 'viz_datasets.csv': 429, 'blender_datasets.csv': 392, 'busybox_datasets.csv': 207, 'gnuplot_datasets.csv': 166, 'irssi_datasets.csv': 521, 'libxml2_datasets.csv': 228, 'lighttpd_datasets.csv': 199, 'marlin_datasets.csv': 619, 'mpsolve_datasets.csv': 192, 'parrot_datasets.csv': 316, 'gimp_datasets.csv': 68}
    nltk.download('stopwords')
    source_directory = r"D:\results\gimp\68\\"
    directory = r"docs\\"
    dataset_csv = r"C:\fanas\2\lda_data\marlin_datasets.csv"
    datasets = pd.read_csv(dataset_csv, delimiter=';')
    train_path = datasets['train']
    test_path = datasets['test']

    doc_directory = source_directory + directory + r'\\'
    queries_directory = source_directory + r'queries\\'
    lda_directory = source_directory + r'lda\\'
    results_directory = source_directory + r'results'
    if not os.path.exists(source_directory):
        os.makedirs(source_directory)
    if not os.path.exists(doc_directory):
        os.makedirs(doc_directory)
    if not os.path.exists(queries_directory):
        os.makedirs(queries_directory)
    if not os.path.exists(lda_directory):
        os.makedirs(lda_directory)
    if not os.path.exists(results_directory):
        os.makedirs(results_directory)

    for f in os.listdir(source_directory):
        if not f.endswith(".dat"):
            continue
        os.remove(os.path.join(source_directory, f))
    for f in os.listdir(doc_directory):
        os.remove(os.path.join(doc_directory, f))
    for f in os.listdir(queries_directory):
        os.remove(os.path.join(queries_directory, f))
    for f in os.listdir(lda_directory):
        os.remove(os.path.join(lda_directory, f))
    for f in os.listdir(results_directory):
        os.remove(os.path.join(results_directory, f))
    print("Working in: " + source_directory)

    assets, labels, content, feature_count_test, features = parse_txt(train_path[68])

    print("Generating queries...")

    for i in range(len(features)):
        with open(queries_directory + str(i) + '.txt', 'w') as f:
            f.write(list(features)[i])
    
    if os.path.exists(source_directory + 'model'):
        lda_model = load_model(source_directory + 'model')
    else:
        pass

    print("Processing train data...")
    # Preprocess data
    data = preprocess(content)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = data_words_nostops#lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized


    print("Generating vocab...")
    vocab = []

    for t in range(len(texts)):
        for word in texts[t]:
            if word not in vocab:
                vocab.append(word)
    vocab = sorted(vocab, key=str.lower)

    with open(lda_directory + 'vocab.dat', 'w') as f:
        for word in vocab:
            f.write(word +'\n')

    print("Fetching model topics...")

    with open(lda_directory + 'words.dat', 'w') as f:
        for i in lda_model.show_topics(num_topics=lda_model.num_topics, num_words=len(vocab), formatted=False):
            tup = SortTuple(i[1])
            for j in tup:
                f.write(str(j[1]) + ' ')
            f.write('\n')

    assets, labels, content, feature_count, features = parse_txt(test_path[68])

    print("Processing test data...")

    # Preprocess data
    data = preprocess(content)

    # Tokenize words
    data_words = list(sentences_to_words(data))

    # Remove Stop Words
    data_words_nostops = remove_stopwords(data_words)

    # Do lemmatization keeping only nouns
    data_lemmatized = data_words_nostops#lemmatization(data_words_nostops)

    # Create Dictionary
    id2word = corpora.Dictionary(data_lemmatized)

    # Create Corpus
    texts = data_lemmatized

    print("Generating documents...")

    for t in range(len(texts)):
        with open(doc_directory + str(t) + ".txt", 'w') as g:
            for word in texts[t]:
                g.write(word + "\n")

    print("Generating files list...")

    with open(lda_directory + 'files.dat', 'w') as f:
        for t in range(len(content)):
            f.write("0 " + str(t) + ".txt 0\n")

    print("Fetching mapping data...\n")
        
    with open(lda_directory + 'theta.dat', 'w') as f, open(source_directory + 'estimation.txt', 'r') as g:
        for line in g:
            if line.startswith('['):
                for character in ['[', ']', '(', ')', '\n']:
                    line = line.replace(character, "")
                array = line.split(', ')[1::2]
                for e in array:
                    f.write(e + ' ')
                f.write('\n')

    subprocess.call(["C:/cygwin64/bin/bash.exe", r"D:\LDA\lucene-lda-master\bin\indexDirectory", str(doc_directory), source_directory + r'index', source_directory + 'ldaHelper.obj', '--ldaConfig', str(feature_count_test) + ',' + lda_directory])
    subprocess.call(["C:/cygwin64/bin/bash.exe", r"D:\LDA\lucene-lda-master\bin\queryWithLda", source_directory + r'index', source_directory + 'ldaHelper.obj', queries_directory, results_directory, '--K', str(feature_count_test)])
